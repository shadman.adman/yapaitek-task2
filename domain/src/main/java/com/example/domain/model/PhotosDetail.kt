package com.example.domain.model


import com.google.gson.annotations.SerializedName

data class PhotosDetail(
    @SerializedName("photo")
    val photo: Photo? = null,
    @SerializedName("stat")
    val stat: String? = null
) {
    data class Photo(
        @SerializedName("comments")
        val comments: Comments? = null,
        @SerializedName("dates")
        val dates: Dates? = null,
        @SerializedName("dateuploaded")
        val dateuploaded: String? = null,
        @SerializedName("description")
        val description: Description? = null,
        @SerializedName("editability")
        val editability: Editability? = null,
        @SerializedName("farm")
        val farm: Int? = null,
        @SerializedName("id")
        val id: String? = null,
        @SerializedName("isfavorite")
        val isfavorite: Int? = null,
        @SerializedName("license")
        val license: Int? = null,
        @SerializedName("media")
        val media: String? = null,
        @SerializedName("notes")
        val notes: Notes? = null,
        @SerializedName("originalformat")
        val originalformat: String? = null,
        @SerializedName("originalsecret")
        val originalsecret: String? = null,
        @SerializedName("owner")
        val owner: Owner? = null,
        @SerializedName("people")
        val people: People? = null,
        @SerializedName("publiceditability")
        val publiceditability: Publiceditability? = null,
        @SerializedName("rotation")
        val rotation: Int? = null,
        @SerializedName("safety_level")
        val safetyLevel: Int? = null,
        @SerializedName("secret")
        val secret: String? = null,
        @SerializedName("server")
        val server: String? = null,
        @SerializedName("tags")
        val tags: Tags? = null,
        @SerializedName("title")
        val title: Title? = null,
        @SerializedName("urls")
        val urls: Urls? = null,
        @SerializedName("usage")
        val usage: Usage? = null,
        @SerializedName("views")
        val views: Int? = null,
        @SerializedName("visibility")
        val visibility: Visibility? = null
    ) {
        data class Comments(
            @SerializedName("_content")
            val content: Int? = null
        )

        data class Dates(
            @SerializedName("lastupdate")
            val lastupdate: String? = null,
            @SerializedName("posted")
            val posted: String? = null,
            @SerializedName("taken")
            val taken: String? = null,
            @SerializedName("takengranularity")
            val takengranularity: Int? = null,
            @SerializedName("takenunknown")
            val takenunknown: Int? = null
        )

        data class Description(
            @SerializedName("_content")
            val content: String? = null
        )

        data class Editability(
            @SerializedName("canaddmeta")
            val canaddmeta: Int? = null,
            @SerializedName("cancomment")
            val cancomment: Int? = null
        )

        data class Notes(
            @SerializedName("note")
            val note: List<Any?>? = null
        )

        data class Owner(
            @SerializedName("iconfarm")
            val iconfarm: Int? = null,
            @SerializedName("iconserver")
            val iconserver: String? = null,
            @SerializedName("location")
            val location: String? = null,
            @SerializedName("nsid")
            val nsid: String? = null,
            @SerializedName("path_alias")
            val pathAlias: String? = null,
            @SerializedName("realname")
            val realname: String? = null,
            @SerializedName("username")
            val username: String? = null
        )

        data class People(
            @SerializedName("haspeople")
            val haspeople: Int? = null
        )

        data class Publiceditability(
            @SerializedName("canaddmeta")
            val canaddmeta: Int? = null,
            @SerializedName("cancomment")
            val cancomment: Int? = null
        )

        data class Tags(
            @SerializedName("tag")
            val tag: List<Any?>? = null
        )

        data class Title(
            @SerializedName("_content")
            val content: String? = null
        )

        data class Urls(
            @SerializedName("url")
            val url: List<Url?>? = null
        ) {
            data class Url(
                @SerializedName("_content")
                val content: String? = null,
                @SerializedName("type")
                val type: String? = null
            )
        }

        data class Usage(
            @SerializedName("canblog")
            val canblog: Int? = null,
            @SerializedName("candownload")
            val candownload: Int? = null,
            @SerializedName("canprint")
            val canprint: Int? = null,
            @SerializedName("canshare")
            val canshare: Int? = null
        )

        data class Visibility(
            @SerializedName("isfamily")
            val isfamily: Int? = null,
            @SerializedName("isfriend")
            val isfriend: Int? = null,
            @SerializedName("ispublic")
            val ispublic: Int? = null
        )
    }
}