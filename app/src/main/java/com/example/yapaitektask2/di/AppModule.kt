package com.example.yapaitektask2.di

import android.nfc.Tag
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import com.example.data.common.coroutine.CoroutineContextProvider
import com.example.yapaitektask2.routing.AppBottomSheetNavigator
import com.example.yapaitektask2.routing.AppNavigator
import com.example.yapaitektask2.routing.BottomSheetNavigator
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import org.koin.dsl.module

val appModule = module {
    single { CoroutineContextProvider() }
    single { (activity: AppCompatActivity) -> AppNavigator(activity) }
    single { (fragmentManager: FragmentManager, bottomSheet:BottomSheetDialogFragment) -> AppBottomSheetNavigator(fragmentManager,bottomSheet)
    }
}