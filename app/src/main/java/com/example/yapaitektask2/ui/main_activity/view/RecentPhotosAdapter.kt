package com.example.yapaitektask2.ui.main_activity.view

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.domain.model.RecentPhotos
import com.example.yapaitektask2.databinding.ItemImageListBinding
import com.example.yapaitektask2.databinding.ItemLoadingBinding


class RecentPhotosAdapter(val context: Context,
                          val dataList: ArrayList<RecentPhotos.Photos.Photo>,
                          val photoItemClickListener :PhotoItemClickListener):RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val VIEW_TYPE_ITEM = 0
    private val VIEW_TYPE_LOADING = 1

    class ViewHolderItem(val binding: ItemImageListBinding) : RecyclerView.ViewHolder(binding.root)
    class ViewHolderLoading(val binding: ItemLoadingBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == VIEW_TYPE_ITEM) {
            val layoutInflater = LayoutInflater.from(context)
            val binding = ItemImageListBinding.inflate(layoutInflater, parent, false)
            ViewHolderItem(binding)
        } else {
            val layoutInflater = LayoutInflater.from(context)
            val binding = ItemLoadingBinding.inflate(layoutInflater, parent, false)
            ViewHolderLoading(binding)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolderItem) {
            populateItemRows(holder, position)
        } else if (holder is ViewHolderLoading) {
            showLoadingView(holder, position)
        }
    }

    private fun showLoadingView(viewHolder: ViewHolderLoading, position: Int) {
        //ProgressBar would be displayed
    }

    private fun populateItemRows(viewHolder: ViewHolderItem, position: Int) {
        val item= dataList.get(position)
//        val circularProgressDrawable = CircularProgressDrawable(context)
        val imageUrl="https://farm${item.farm}.staticflickr.com/${item.server}/${item.id}_${item.secret}.jpg"
//        circularProgressDrawable.strokeWidth = 5f
//        circularProgressDrawable.centerRadius = 30f
//        circularProgressDrawable.start()
        Glide.with(context)
            .load(imageUrl)
//            .placeholder(circularProgressDrawable)
            .dontAnimate()
            .into(viewHolder.binding.imageView)
        viewHolder.binding.textViewTitle.text=item.title
        viewHolder.binding.textViewOwner.text=item.owner
        viewHolder.binding.root.setOnClickListener {
            photoItemClickListener.onClickedImage(item.id!!, imageUrl)
        }
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (dataList[position] == null) VIEW_TYPE_LOADING else VIEW_TYPE_ITEM
    }

    interface PhotoItemClickListener {
        fun onClickedImage(imageId: String,imageUrl:String)
    }

}