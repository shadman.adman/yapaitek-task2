package com.example.yapaitektask2.common

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.text.format.DateFormat
import androidx.core.content.ContextCompat.startActivity
import java.text.SimpleDateFormat
import java.util.*

/**
 * Convert time stamp to local date
 */
 fun getDate(timeStamp: Long): String? {
    return try {
        val calendar = Calendar.getInstance(Locale.ENGLISH)
        calendar.timeInMillis = timeStamp * 1000L
        val date = DateFormat.format("dd-MM-yyyy",calendar).toString()
        return date
    } catch (ex: Exception) {
        "xx"
    }
}

/**
 * Opening URL
 */
fun openWebURL(inURL: String?,context:Context) {
    val browse = Intent(Intent.ACTION_VIEW, Uri.parse(inURL))
    context.startActivity(browse)
}

