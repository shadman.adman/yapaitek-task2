package com.example.data.networking

import com.example.data.networking.model.photos_detail_model.PhotosDetailModel
import com.example.data.networking.model.recent_photos_model.RecentPhotosModel
import com.example.domain.model.PhotosDetail
import com.example.domain.model.RecentPhotos
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface FlickrApi {
//    @GET("services/rest/")
//    suspend fun getRecentPhoto(@Query("method") method:String,
//                               @Query("format") format:String,
//                               @Query("nojsoncallback") noJsonCallback:Int,
//                               @Query("per_page") perPage:Int,
//                               @Query("page") page:Int)
//            : Response<RecentPhotosModel>

        @GET("services/rest/")
    suspend fun getRecentPhoto(@QueryMap params:Map<String,String>)
            : Response<RecentPhotosModel>

//    @GET("services/rest/")
//    suspend fun getPhotoDetails(@Query("method") method:String,
//                                @Query("photo_id") photoId:String,
//                                @Query("format") format:String,
//                                @Query("nojsoncallback") noJsonCallback:Int)
//            :Response<PhotosDetailModel>

        @GET("services/rest/")
    suspend fun getPhotoDetails(@QueryMap params: Map<String, String>)
            :Response<PhotosDetailModel>

}