package com.example.yapaitektask2.ui.main_activity.presentation

import com.example.domain.interaction.GetRecentPhotosUseCase
import com.example.domain.model.RecentPhotos
import com.example.yapaitektask2.ui.base.BaseViewModel
import com.example.yapaitektask2.ui.main_activity.RecentPhotosViewEffects
import com.example.domain.model.onFailure
import com.example.domain.model.onSuccess
import com.example.yapaitektask2.ui.base.Error
import com.example.yapaitektask2.ui.base.Success

class RecentPhotosViewModel(private val getRecentPhotosUseCase: GetRecentPhotosUseCase):
BaseViewModel<RecentPhotos,RecentPhotosViewEffects>(){
    fun getRecentPhotos(params:Map<String,String>)
    =executeUseCase {
        getRecentPhotosUseCase(params)
            .onSuccess { _viewState.value = Success(it) }
            .onFailure { _viewState.value = Error(it.throwable) }
    }
}