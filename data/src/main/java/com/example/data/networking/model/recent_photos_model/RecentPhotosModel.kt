package com.example.data.networking.model.recent_photos_model


import com.example.data.networking.base.DomainMapper
import com.example.domain.model.RecentPhotos
import com.google.gson.annotations.SerializedName

data class RecentPhotosModel(
    @SerializedName("photos")
    val photos: RecentPhotos.Photos? = null,
    @SerializedName("stat")
    val stat: String? = null
) : DomainMapper<RecentPhotos> {
    override fun mapToDomainModel()= RecentPhotos(photos,stat)
}