package com.example.yapaitektask2.ui.info.view

import android.os.Bundle
import com.example.yapaitektask2.R
import com.example.yapaitektask2.common.extensions.onClick
import com.example.yapaitektask2.common.openWebURL
import com.example.yapaitektask2.ui.base.BaseBottomSheet
import kotlinx.android.synthetic.main.bottom_sheet_info.*

private const val PHOTO_ID = "PHOTO_ID"
private const val IMAGE_URL = "IMAGE_URL"
class BottomSheetInfo:BaseBottomSheet() {
    override fun viewReady() {
        button_go_to_repo.onClick {
            openWebURL("https://gitlab.com/shadman.adman/yapaitek-task2",mContext)
        }
    }

    override fun getLayout()= R.layout.bottom_sheet_info

}