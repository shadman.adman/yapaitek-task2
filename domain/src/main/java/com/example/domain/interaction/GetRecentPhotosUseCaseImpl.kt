package com.example.domain.interaction

import com.example.domain.repository.PhotosRepository

class GetRecentPhotosUseCaseImpl(private val photosRepository: PhotosRepository)
    :GetRecentPhotosUseCase{
    override suspend fun invoke(params: Map<String, String>)=photosRepository.getRecentPhotos(params)
}