package com.example.domain.repository

import com.example.domain.model.PhotosDetail
import com.example.domain.model.Result

interface PhotosDetailRepository {
    suspend fun getPhotosDetail(params:Map<String,String>): Result<PhotosDetail>
}