package com.example.yapaitektask2.ui.splash_screen.view

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.example.yapaitektask2.R
import com.example.yapaitektask2.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_splash_screen.*

const val DELAY_FOR_SPLASH_SCREEN=2500L
class SplashActivity:BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        setupSplashAnimation()
        Handler(Looper.myLooper()!!).postDelayed({
            appNavigator.showMainActivity()
            finish()
        }, DELAY_FOR_SPLASH_SCREEN)
    }

    private fun setupSplashAnimation(){
        logo.animate().translationY(0F).duration=1500
        logo.animate().alpha(1.0F).duration=1000
        title_app.animate().alpha(1.0F).duration=2000
        sub_title.animate().alpha(1.0F).duration=2500
    }

}