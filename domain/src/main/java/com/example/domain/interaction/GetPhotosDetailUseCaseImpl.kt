package com.example.domain.interaction

import com.example.domain.repository.PhotosDetailRepository
import com.example.domain.repository.PhotosRepository

class GetPhotosDetailUseCaseImpl (private val photosDetailRepository: PhotosDetailRepository)
    :GetPhotosDetailUseCase{
    override suspend fun invoke(params: Map<String, String>)= photosDetailRepository.getPhotosDetail(params)
}