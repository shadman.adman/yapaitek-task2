package com.example.data.repository

import com.example.data.networking.FlickrApi
import com.example.data.networking.base.getData
import com.example.domain.model.Result
import com.example.domain.model.RecentPhotos
import com.example.domain.repository.PhotosRepository

class PhotosRepositoryImpl(
    private val flickrApi: FlickrApi
) : BaseRepository<RecentPhotos>(), PhotosRepository {
    override suspend fun getRecentPhotos(params: Map<String, String>): Result<RecentPhotos> {
        return fetchData(
            dataProvider = {
                flickrApi.getRecentPhoto(params).getData()
            }
        )
    }
}