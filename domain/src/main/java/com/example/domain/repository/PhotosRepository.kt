package com.example.domain.repository

import com.example.domain.model.RecentPhotos
import com.example.domain.model.Result

interface PhotosRepository {
    suspend fun getRecentPhotos(params:Map<String,String>):Result<RecentPhotos>
}