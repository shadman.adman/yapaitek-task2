package com.example.data.networking.model.photos_detail_model


import com.example.data.networking.base.DomainMapper
import com.example.domain.model.PhotosDetail
import com.google.gson.annotations.SerializedName

data class PhotosDetailModel(
    @SerializedName("photo")
    val photo: PhotosDetail.Photo? = null,
    @SerializedName("stat")
    val stat: String? = null
): DomainMapper<PhotosDetail> {

    override fun mapToDomainModel()= PhotosDetail(photo ,stat)
}