package com.example.yapaitektask2.ui.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.yapaitektask2.common.extensions.launch
import com.example.data.common.coroutine.CoroutineContextProvider
import com.example.data.common.utils.Connectivity
import kotlinx.coroutines.cancel
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.io.IOException

abstract class BaseViewModel<T:Any,E>:ViewModel(),KoinComponent {
    protected val coroutineContext: CoroutineContextProvider by inject()
    private val connectivity: Connectivity by inject()

    protected val _viewState = MutableLiveData<ViewState<T>>()
    val viewState: LiveData<ViewState<T>>
        get() = _viewState

    protected val _viewEffects = MutableLiveData<E>()
    val viewEffects: LiveData<E>
        get() = _viewEffects

    protected fun executeUseCase(action: suspend () -> Unit, noInternetAction: () -> Unit) {
        _viewState.value = Loading()
        if (connectivity.hasNetworkAccess()) {
            launch { action() }
        } else {
            noInternetAction()
        }
    }

    protected fun executeUseCase(action: suspend () -> Unit) {
        _viewState.value = Loading()
        launch {
            try {
                action()
            }catch (e:Exception){
                print(e.localizedMessage)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        launch { }.cancel()
    }
}