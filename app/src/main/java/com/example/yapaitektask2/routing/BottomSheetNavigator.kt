package com.example.yapaitektask2.routing

interface BottomSheetNavigator {
    fun showPhotoDetailsBottomSheet(photoId:String,photoUrl:String,tag:String)
    fun showInfoDetailsBottomSheet(tag:String)
}