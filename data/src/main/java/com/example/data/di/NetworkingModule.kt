package com.example.data.di

import com.example.data.networking.FlickrApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.BuildConfig
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import se.akerfeldt.okhttp.signpost.OkHttpOAuthConsumer
import se.akerfeldt.okhttp.signpost.SigningInterceptor
import java.util.concurrent.TimeUnit

// URL
private const val BASE_URL = "https://www.flickr.com/"
// Authorization
private const val KEY="d5da408e45395f2a9d0e99489cca024b"
private const val SECRET="e32bf69d8d7cfbb4"
private val consumer = OkHttpOAuthConsumer(KEY,SECRET)



val networkingModule= module {
    single { GsonConverterFactory.create() as Converter.Factory }
    //single { HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)}
    //single { SigningInterceptor(consumer) }
    single {
        OkHttpClient.Builder().apply {
                addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .callTimeout(10,TimeUnit.SECONDS)
                .addInterceptor(SigningInterceptor(consumer))
        }.build()
    }
    single {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(get())
            .addConverterFactory(get())
            .build()
    }

    single { get<Retrofit>().create(FlickrApi::class.java) }
}