package com.example.yapaitektask2.routing

import androidx.fragment.app.FragmentManager
import com.example.yapaitektask2.common.extensions.showBottomSheet
import com.example.yapaitektask2.ui.photo_details.view.BottomSheetPhotoDetails
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class AppBottomSheetNavigator(
private val fragmentManager: FragmentManager,
private val bottomSheetDialogFragment: BottomSheetDialogFragment
) :BottomSheetNavigator{
    override fun showPhotoDetailsBottomSheet(photoId: String, photoUrl: String, tag: String)=
        showBottomSheet(
            bottomSheetDialogFragment, fragmentManager, tag
        )
    override fun showInfoDetailsBottomSheet(tag: String) {
    }
}