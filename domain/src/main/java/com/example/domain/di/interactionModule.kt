package com.example.domain.di

import com.example.domain.interaction.GetPhotosDetailUseCase
import com.example.domain.interaction.GetPhotosDetailUseCaseImpl
import com.example.domain.interaction.GetRecentPhotosUseCase
import com.example.domain.interaction.GetRecentPhotosUseCaseImpl
import org.koin.dsl.module

val interactionModule= module {
    factory<GetRecentPhotosUseCase> { GetRecentPhotosUseCaseImpl(get()) }
    factory<GetPhotosDetailUseCase> { GetPhotosDetailUseCaseImpl(get()) }
}