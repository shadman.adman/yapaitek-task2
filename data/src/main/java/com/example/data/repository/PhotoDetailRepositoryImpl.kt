package com.example.data.repository

import com.example.data.networking.FlickrApi
import com.example.data.networking.base.getData
import com.example.domain.model.PhotosDetail
import com.example.domain.model.Result
import com.example.domain.repository.PhotosDetailRepository

class PhotoDetailRepositoryImpl(
    private val flickrApi: FlickrApi
) : BaseRepository<PhotosDetail>(), PhotosDetailRepository {
    override suspend fun getPhotosDetail(params: Map<String, String>): Result<PhotosDetail> {
        return fetchData(
            dataProvider = {
                flickrApi.getPhotoDetails(params).getData()
            }
        )
    }

}