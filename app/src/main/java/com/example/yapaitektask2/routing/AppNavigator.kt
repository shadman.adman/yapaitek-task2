package com.example.yapaitektask2.routing

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import com.example.yapaitektask2.ui.base.BaseActivity
import com.example.yapaitektask2.ui.main_activity.view.MainActivity

class AppNavigator(private val activity: AppCompatActivity)
    : Navigator {
    override fun showMainActivity()=navigateTo(getIntent<MainActivity>())

    private fun navigateTo(intent: Intent) = activity.startActivity(intent)

    private inline fun <reified T : BaseActivity> getIntent() = Intent(activity, T::class.java)

}