package com.example.yapaitektask2.ui.main_activity.view

import android.os.Bundle
import androidx.annotation.NonNull
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.domain.model.RecentPhotos
import com.example.yapaitektask2.R
import com.example.yapaitektask2.common.extensions.onClick
import com.example.yapaitektask2.common.extensions.snackbar
import com.example.yapaitektask2.common.extensions.subscribe
import com.example.yapaitektask2.ui.base.*
import com.example.yapaitektask2.ui.info.view.BottomSheetInfo
import com.example.yapaitektask2.ui.main_activity.presentation.RecentPhotosViewModel
import com.example.yapaitektask2.ui.photo_details.view.BottomSheetPhotoDetails
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity:BaseActivity(),RecentPhotosAdapter.PhotoItemClickListener {
    private val viewModel:RecentPhotosViewModel by viewModel()
    private lateinit var adapter: RecentPhotosAdapter
    private var isLoading = false
    companion object{
        private var dataList=ArrayList<RecentPhotos.Photos.Photo>()
        private var mTotalItemsCount=0
        private var currentPage=1
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        subscribeToData()
        image_button_info.onClick{
            BottomSheetInfo().show(supportFragmentManager,"INFO")
        }
        loadData()
    }
    private fun subscribeToData() {
        viewModel.viewState.subscribe(this, ::handleViewState)
    }

    private fun handleViewState(viewState: ViewState<RecentPhotos>) {
        when (viewState) {
            is Loading ->if (currentPage==1) showLoading(progress_circular)
            is Success -> showData(viewState.data)
            is Error -> handleError(viewState.error.localizedMessage)
            is NoInternetState -> showNoInternetError()
        }
    }

    private fun handleError(error: String) {
        hideLoading(progress_circular)
        showError(error, root)
    }

    private fun showNoInternetError() {
        hideLoading(progress_circular)
        snackbar(getString(R.string.please_check_your_internet_connection), root)
    }

    private fun showData(recentPhotosModel: RecentPhotos){
        hideLoading(progress_circular_load_more)
        isLoading=false
        mTotalItemsCount=recentPhotosModel.photos!!.total!!.toInt()
        dataList.addAll(recentPhotosModel.photos?.photo as ArrayList<RecentPhotos.Photos.Photo>)
        initScrollListener()
        if (currentPage==1) {
            hideLoading(progress_circular)
            adapter = RecentPhotosAdapter(
                this,
                dataList,
                this
            )
            photo_list.adapter = adapter
        }
        adapter.notifyDataSetChanged()
    }

    private fun loadData(){
        val map: MutableMap<String, String> = HashMap()
        map["format"] = "json"
        map["method"] = "flickr.photos.getRecent"
        map["nojsoncallback"]="1"
        map["per_page"]="20"
        map["page"]= currentPage.toString()
        viewModel.getRecentPhotos(
            map
        )
    }

    private fun initScrollListener() {
        photo_list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(@NonNull recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager?
                if (dy>0) {
                    if ((!isLoading && linearLayoutManager?.findLastCompletelyVisibleItemPosition()!! >=
                                adapter.itemCount - 3) && dataList.size < mTotalItemsCount) {
                        currentPage++
                        showLoading(progress_circular_load_more)
                        loadData()
                        isLoading = true
                    }
                }
            }
        })
    }

    override fun onClickedImage(imageId: String, imageUrl: String) {
        BottomSheetPhotoDetails.newInstance(imageId,imageUrl).show(supportFragmentManager,"PHOTO_DETAILS")
    }
}