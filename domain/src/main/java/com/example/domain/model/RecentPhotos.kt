package com.example.domain.model


import com.google.gson.annotations.SerializedName

data class RecentPhotos(
    @SerializedName("photos")
    val photos: Photos? = null,
    @SerializedName("stat")
    val stat: String? = null
) {
    data class Photos(
        @SerializedName("page")
        val page: Int? = null,
        @SerializedName("pages")
        val pages: Int? = null,
        @SerializedName("perpage")
        val perpage: Int? = null,
        @SerializedName("photo")
        val photo: List<Photo?>? = null,
        @SerializedName("total")
        val total: String? = null
    ) {
        data class Photo(
            @SerializedName("farm")
            val farm: Int? = null,
            @SerializedName("id")
            val id: String? = null,
            @SerializedName("isfamily")
            val isfamily: Int? = null,
            @SerializedName("isfriend")
            val isfriend: Int? = null,
            @SerializedName("ispublic")
            val ispublic: Int? = null,
            @SerializedName("owner")
            val owner: String? = null,
            @SerializedName("secret")
            val secret: String? = null,
            @SerializedName("server")
            val server: String? = null,
            @SerializedName("title")
            val title: String? = null
        )
    }
}