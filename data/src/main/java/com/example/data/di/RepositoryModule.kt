package com.example.data.di

import com.example.data.common.utils.Connectivity
import com.example.data.common.utils.ConnectivityImpl
import com.example.data.repository.PhotoDetailRepositoryImpl
import com.example.data.repository.PhotosRepositoryImpl
import com.example.domain.repository.PhotosDetailRepository
import com.example.domain.repository.PhotosRepository
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val repositoryModule= module {
    factory<Connectivity> { ConnectivityImpl(androidContext()) }
    factory<PhotosRepository> { PhotosRepositoryImpl(get()) }
    factory<PhotosDetailRepository> { PhotoDetailRepositoryImpl(get()) }
}