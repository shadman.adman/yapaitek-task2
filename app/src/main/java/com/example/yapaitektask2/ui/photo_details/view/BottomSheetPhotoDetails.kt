package com.example.yapaitektask2.ui.photo_details.view

import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import com.example.domain.model.PhotosDetail
import com.example.yapaitektask2.R
import com.example.yapaitektask2.common.extensions.snackbar
import com.example.yapaitektask2.common.extensions.subscribe
import com.example.yapaitektask2.common.getDate
import com.example.yapaitektask2.ui.base.*
import com.example.yapaitektask2.ui.photo_details.presentation.PhotoDetailsViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.progress_circular
import kotlinx.android.synthetic.main.bottom_sheet_photo_details.*
import org.koin.androidx.viewmodel.ext.android.viewModel

private const val PHOTO_ID = "PHOTO_ID"
private const val IMAGE_URL = "IMAGE_URL"
class BottomSheetPhotoDetails:BaseBottomSheet() {
    private val viewModel:PhotoDetailsViewModel by viewModel()
    private var photoId = ""
    private var imageUrl = ""
    override fun viewReady() {
        subscribeToData()
        val map: MutableMap<String, String> = HashMap()
        map["format"] = "json"
        map["method"] = "flickr.photos.getInfo"
        map["nojsoncallback"]="1"
        map["photo_id"]=photoId
        viewModel.getPhotoDetails(map)
    }

    override fun getLayout()= R.layout.bottom_sheet_photo_details

    private fun subscribeToData() {
        viewModel.viewState.subscribe(this, ::handleViewState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            photoId = it.getString(PHOTO_ID, "")
            imageUrl = it.getString(IMAGE_URL, "")
        }
    }

    private fun handleViewState(viewState: ViewState<PhotosDetail>) {
        when (viewState) {
            is Loading -> showLoading(progress_circular)
            is Success -> showData(viewState.data)
            is Error -> handleError(viewState.error.localizedMessage)
            is NoInternetState -> showNoInternetError()
        }
    }

    private fun handleError(error: String) {
        hideLoading(progress_circular)
        showError(error, root)
    }

    private fun showNoInternetError() {
        hideLoading(progress_circular)
        snackbar(getString(R.string.please_check_your_internet_connection), root)
    }

    private fun showData(photosDetailModel: PhotosDetail){
        hideLoading(progress_circular)
        val owner_icon="https://farm${photosDetailModel.photo?.owner?.iconfarm}.staticflickr.com/${photosDetailModel.photo?.owner?.iconserver}/buddyicons/${photosDetailModel.photo?.owner?.nsid}.jpg"

        Glide.with(context!!)
            .load(imageUrl)
            .dontAnimate()
            .into(image_view)

        text_view_title.text = photosDetailModel.photo?.title?.content
        text_view_description.text = photosDetailModel.photo?.description?.content
        if (photosDetailModel.photo?.owner?.realname!="")
        text_view_owner.text=context?.getString(R.string.owner,photosDetailModel.photo?.owner?.realname)
        text_view_views.text = context?.getString(R.string.views, photosDetailModel.photo?.views.toString())
        text_view_comments.text =
            context?.getString(R.string.comments, photosDetailModel.photo?.comments?.content)
        text_view_posted_date.text =
            context?.getString(R.string.taken_on, getDate(photosDetailModel.photo?.dates?.posted!!.toLong()))
        if (photosDetailModel.photo?.owner?.iconfarm!=0){
            Glide.with(context!!)
                .load(owner_icon)
                .circleCrop()
                .dontAnimate()
                .into(image_view_owner_icon)
            text_view_username.text=photosDetailModel.photo?.owner?.username
            layout_user_details.visibility= View.VISIBLE
        }
    }

    companion object {
        fun newInstance(photoId: String, imageUrl: String):
                BottomSheetPhotoDetails =
            BottomSheetPhotoDetails().apply {
                arguments = Bundle().apply {
                    putString(PHOTO_ID, photoId)
                    putString(IMAGE_URL, imageUrl)
                }
            }
    }

}