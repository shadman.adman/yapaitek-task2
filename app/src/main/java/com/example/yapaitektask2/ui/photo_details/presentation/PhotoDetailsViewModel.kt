package com.example.yapaitektask2.ui.photo_details.presentation

import com.example.domain.interaction.GetPhotosDetailUseCase
import com.example.domain.model.PhotosDetail
import com.example.domain.model.onFailure
import com.example.domain.model.onSuccess
import com.example.yapaitektask2.ui.base.BaseViewModel
import com.example.yapaitektask2.ui.base.Error
import com.example.yapaitektask2.ui.base.Success
import com.example.yapaitektask2.ui.photo_details.PhotoDetailsViewEffects

class PhotoDetailsViewModel(private val getPhotosDetailUseCase: GetPhotosDetailUseCase)
    :BaseViewModel<PhotosDetail,PhotoDetailsViewEffects>(){
        fun getPhotoDetails(params:Map<String,String>)=
            executeUseCase {
                getPhotosDetailUseCase(params)
                    .onSuccess { _viewState.value = Success(it) }
                    .onFailure { _viewState.value = Error(it.throwable) }
            }
}