package com.example.domain.interaction

import com.example.domain.base.BaseUseCase
import com.example.domain.model.RecentPhotos
import com.example.domain.model.Result
interface GetRecentPhotosUseCase : BaseUseCase<String, RecentPhotos> {
    override suspend fun invoke(params: Map<String, String>): Result<RecentPhotos>
}