# YapAiTek Task

This is a task given by **YapAiTek** to be hired. The programming language used for this task is **Kotlin**.
In this project I used the following concepts:


- **Clean Architecture** is the main architecture
- **MVVM** is the ui architecture pattern
- **Kotlin Coroutines** for doing async works
- **Retrofit** as networking tool
- **Okhttp logging-interceptor** to logging the http requests
- **Gson** for decoding json
- **Glide** for loading images
- **Koin** for dependency injection
- **Oauth01** is used for authentication
- **Room** is also added for cashing(not implemented)

Note1: The apk release is avilable in [this](https://gitlab.com/shadman.adman/yapaitek-task2/-/tree/master/app/release) location. fell free to try it.

Note2: Please connect to a vpn while using the app. Flickr is unfortunately filtered.

