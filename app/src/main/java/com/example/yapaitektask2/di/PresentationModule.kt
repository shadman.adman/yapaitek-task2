package com.example.yapaitektask2.di

import com.example.yapaitektask2.ui.main_activity.presentation.RecentPhotosViewModel
import com.example.yapaitektask2.ui.photo_details.presentation.PhotoDetailsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val presentationModule= module {
    viewModel { RecentPhotosViewModel(get()) }
    viewModel { PhotoDetailsViewModel(get()) }
}