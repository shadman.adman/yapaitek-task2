package com.example.domain.interaction

import com.example.domain.base.BaseUseCase
import com.example.domain.model.PhotosDetail
import com.example.domain.model.Result

interface GetPhotosDetailUseCase:BaseUseCase<String,PhotosDetail> {
    override suspend fun invoke(params: Map<String, String>): Result<PhotosDetail>
}